﻿using System.Web.Mvc;
using System.Web.Routing;

namespace GraphsChart
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "{controller}/{action}/{id}",
                new {controller = "Charts", action = "Index", id = UrlParameter.Optional}
                );
        }
    }
}