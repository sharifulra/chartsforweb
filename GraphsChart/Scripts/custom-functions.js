﻿//$(document).ready(function() {


//});
function OpenPopUp(pageName) {
    window.open("../ReportViewerForm/" + pageName, "_blank", "WIDTH=1024,HEIGHT=768,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
    //window.open("~/ReportViewerForm/BasicReportViewer.aspx", "_blank", "WIDTH=1080,HEIGHT=790,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
}

function LoadDate(controlName) {
    jQuery(controlName).datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-M-yyyy'
    });
}


function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    var dayData = dt.getDate();
    if (dayData < 10) {
        dayData = "0" + dayData;
    }
    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var rDate = dayData + "-" + monthNames[(dt.getMonth())] + "-" + dt.getFullYear();
    return rDate;
}

function ValidationColorChange(controlName, divName, isDropdown) {

    var flag = 'input';
    if (isDropdown == true) {
        flag = 'change';
    }

    $('#' + divName + '').addClass("has-warning");
    $('#' + controlName + '').addClass("form-control-warning");
    $('#' + controlName + '').bind(flag, function () {
        $('#' + divName + '').removeClass("has-warning");
        $('#' + controlName + '').removeClass("form-control-warning");
    });
}

function ShowMessage(msgText) {
    swal(msgText);
}

function AddDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function DateFormatChange(dateParam) {
    var todaydate = new Date(dateParam);  //pass val varible in Date(val)
    var dd = todaydate.getDate();
    //var mm = todaydate.getMonth() + 1; //January is 0!
    var mm = todaydate.getMonth();
    var yyyy = todaydate.getFullYear();
    if (dd < 10) { dd = '0' + dd }
    //if (mm < 10) { mm = '0' + mm }
    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = dd + '-' + monthNames[mm] + '-' + yyyy;
    return date;
}