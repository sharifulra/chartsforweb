﻿using System.Collections.Generic;
using System.Web.Mvc;


namespace GraphsChart.Controllers
{
    public class ChartsController : Controller
    {
        // GET: Charts
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Chartjs()
        {

            return View("ChartJs");
        }
        public ActionResult GoogleChart()
        {
            return View("GoogleChart");
        }

        public ActionResult Morris()
        {
            return View("Morris");
        }



    }
}